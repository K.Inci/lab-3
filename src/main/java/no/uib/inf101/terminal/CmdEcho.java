package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
         String cmdString = "";
        for (String arg: args){
            cmdString +=arg + " ";
        }
        return cmdString;
    }

    @Override
    public String getName() {
        
        return "echo";
    }
    
}
